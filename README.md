## Description


Documentation theme for [Hugo](https://gohugo.io/) static site generator.
Based on [hugo-theme-scgdoc](https://github.com/scgmlz/hugo-theme-scgdoc)

The theme was initially based on
* [Scientific Computing group at MLZ](https://github.com/scgmlz)
* [hugo-theme-learn - Documentation theme for Hugo](https://github.com/gpospelov/hugo-theme-learn)
* [docDoc - Documentation theme for Hugo](https://themes.gohugo.io/docdock/)
* [Startboostrap Clean Blog](https://themes.gohugo.io/startbootstrap-clean-blog/)


## Key features

* Minimum Hugo version v0.37
* Bootstrap 4 dependent
* Documentation page with automaticly generated breadcrumb and side navigation bar (collapse/expand functionality)
* Page with posts with multi-author support
* Various ways to insert images (galleries, responsivness, automatic resize)
* Python code highlight
