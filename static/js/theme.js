
// operation system detection
function osName() {
  var OSName = "Unknown OS";
  if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
  else if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
  else if (navigator.appVersion.indexOf("X11") != -1) OSName = "Linux";
  else if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
  return OSName
}

// Collapse/extend documentation tree
$('.li-item-icon').click(function () {
  $( this ).toggleClass("fa-minus-square") ;
  $( this ).toggleClass("fa-plus-square") ;
  $( this ).parent().children('ul').toggle() ;
  return false;
});


function leftArrowPressed() {
  document.getElementById("btnNavPrev").click();
}

function rightArrowPressed() {
  document.getElementById("btnNavNext").click();
}

document.onkeydown = function(evt) {
  evt = evt || window.event;
  // ctrlKey altKey shiftKey metaKey
  // if( evt.altKey && evt.shiftKey ) {
  if( evt.ctrlKey ) {
    switch (evt.keyCode) {
    case 37:
      leftArrowPressed();
      break;
    case 39:
      rightArrowPressed();
      break;
    }
  }
};
