+++
title          = "Notes"
weight         = 10
description    = "basis for the design and use of computers"
pdfSelect      = 1
+++


## CSC-HS19

{{%  alert secondary %}}
{{< icon-class icon="fas fa-server" size="fa-4x" class="alert-primary" >}}
{{% /alert %}}

{{%  quote source="[Computer_science](https://en.wikipedia.org/wiki/Computer_science) *(en.wikipedia.org)*" %}}

Computer science is the study of the theory, experimentation, and
engineering that form the basis for the design and use of computers.

{{% /quote %}}



