+++
title          = "HS19"
weight         = 200
description    = "Kalender"
pdfSelect      = 5
+++

## Herbstsemester 2019

{{%  alert secondary %}}
{{< icon-class icon="far fa-calendar" size="fa-4x" class="alert-primary" >}}
{{% /alert %}}

* Unterlagen im ILIAS: [W.MSCIDS_CSC01.H1901](https://elearning.hslu.ch/ilias/ilias.php?ref_id=3932932&cmdClass=ilrepositorygui&cmdNode=vs&baseClass=ilRepositoryGUI)
* Jeweils `Do 08:15 - 11:35`
* Raum: `Z9 ???`



| Nr  | KW | Datum    | Dozent, Themen                          | Links                                                                |
|-----|----|----------|-----------------------------------------|----------------------------------------------------------------------|
| 1   | 38 | Do 19.09 | Prof. Dr. Martin Zimmermann             |                                                                      |
| 2   | 39 | Do 26.09 | Prof. Dr. Martin Zimmermann             |                                                                      |
| 3   | 40 | Do 03.10 | Prof. Dr. Martin Zimmermann             |                                                                      |
| 4.a | 41 | Do 10.10 | Prof. Dr. Martin Zimmermann             |                                                                      |
| 4.b | 41 | Do 10.10 | M. Sc. Bruno Grossniklaus               | [Agenda Part 1]({{< ref "/doc/csc/intro/agenda/part1/_index.md" >}}) |
|     |    |          | * Computersysteme                       |                                                                      |
|     |    |          | * BYOD vorbereiten                      |                                                                      |
| 5   | 42 | Do 17.10 | M. Sc. Bruno Grossniklaus               | [Agenda Part 2]({{< ref "/doc/csc/intro/agenda/part2/_index.md" >}}) |
|     |    |          | * Fallstudie *Virtualisierung*          |                                                                      |
|     |    |          | * Betriebssysteme (allg., Linux)        |                                                                      |
|     |    |          | * Fallstudie *Rechner vernetzen*        |                                                                      |
| 6   | 43 | Do 24.10 | M. Sc. Bruno Grossniklaus               | [Agenda Part 3]({{< ref "/doc/csc/intro/agenda/part3/_index.md" >}}) |
|     |    |          | * Netzwerke (Layer, IP, Subnet          |                                                                      |
|     |    |          | Switch, Router, Firewall)               |                                                                      |
|     |    |          | * grundlegende Tools (editor, git, ssh) |                                                                      |
|     |    |          | * Fallstudie *Server in Netzwerken*     |                                                                      |
| 7   | 44 | Do 31.10 | M. Sc. Bruno Grossniklaus               | [Agenda Part 4]({{< ref "/doc/csc/intro/agenda/part4/_index.md" >}}) |
|     |    |          | * Fallstudie *Server in Netzwerken*     |                                                                      |
|     |    |          | * Virtualisierung                       |                                                                      |
|     |    |          | * Cloud Computing                       |                                                                      |
| ?   | ?  | ?        | Leistungsnachweise                      |                                                                      |


<small>
**Trademarks** \
All product names, logos, and brands are property of their respective
owners. All company, product and service names used in this website
are for identification purposes only. Use of these names, logos, and
brands does not imply endorsement.
</small>
