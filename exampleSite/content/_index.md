+++
title          = "CSC"
description    = "Unterlagen CSC HS19"
#layout="landing"
pdfSelect      = 1
+++

# Computer Science Concepts for Data Scientists


{{% alert theme="secondary" %}}

Ein Konvergenzangebot für Studierende des multidisziplinären
Master-Studiengang [Applied Information and Data
Science](https://www.hslu.ch/de-ch/wirtschaft/studium/master/applied-information-and-data-science)
([Broschüre](https://www.hslu.ch/-/media/campus/common/files/dokumente/w/studium/msc-broschueren/broschuere-master-ids.pdf?la=de-ch)),
die noch nicht über ausreichende Vorkenntnisse in grundlegenden
Informatik-Konzepten verfügen.

{{% /alert %}}



{{%  quote source="[applied-information-and-data-science/module](https://www.hslu.ch/de-ch/wirtschaft/studium/master/applied-information-and-data-science/module/) *(www.hslu.ch)*" %}}

Die Studierenden erhalten eine Einführung in die Grundlagen der
Informatik und lernen ausgewählte Methoden und IT-Werkzeuge vertiefter
kennen. 

Sie werden anhand verschiedener Schwerpunktthemen in die Denk-
und Arbeitsweise der Informatik eingeführt: \
Bedeutung der Informatik in der Informationsgesellschaft, Aufbau und
Funktionsweise von Computersystemen und Netzwerken, Aufbau des
Internets, grundlegende Prinzipien der strukturierten und
automatischen Informationsverarbeitung, Grundlagen der Modellierung
von Systemen usw.

{{% alert primary %}}
Die Themen und Inhalte des Moduls sind bewusst so gewählt, dass sie
die (zukünftigen) Data Scientists befähigen, mit
Spezialistinnen/Spezialisten aus der Informatik einen Fachdialog zu
führen.
{{% /alert %}}

{{% /quote %}}

Schwerpunkte im diesem Teil des Moduls:

* Computersysteme
* Betriebssysteme
* Netzwerke
* Virtualisierung
* Cloud
