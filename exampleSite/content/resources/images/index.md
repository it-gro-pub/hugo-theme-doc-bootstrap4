---
title:       "headless-images"
headless:    true
resources:

   # default for any
   # ##################################################

  - src:            "**/*"
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      #thumbOpt:     "256x128"
      thumbOpt:     "512x256"
      #thumbOpt:     "1024x768"
      caption:      "%%B%%n%%l%%n%%O %%A"
      captionShort: "%%O%%n%%l"
      #caption:      "%%f"
      #captionShort: "%%B"

               # **/
               # ##################################################

  - src:           it-gro/keyboard/*.jpg
    title:         Keyboard
    params:
      origin:       Screenshot
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch



               # en.wikipedia.org
               # ##################################################

  - src:           en.wikipedia.org/osi_model.jpg
    title:         OSI Model
    params:
      origin:       en.wikipedia.org
      originlink:   https://en.wikipedia.org/wiki/OSI_model
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      #attrby:
      #attrlink:

               # de.wikipedia.org
               # ##################################################

  - src:           de.wikipedia.org/OSI-Modell.jpg
    title:         OSI Model
    params:
      origin:       de.wikipedia.org
      originlink:   https://en.wikipedia.org/wiki/OSI_model
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      #attrby:
      #attrlink:

  - src:           de.wikipedia.org/ethernet-datenframe.jpg
    title:         Ethernet Datenframe
    params:
      origin:       de.wikipedia.org
      originlink:   https://de.wikipedia.org/wiki/Datenframe
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      #attrby:
      #attrlink:

  - src:           de.wikipedia.org/internetprotokollfamilie-osi-tcp-ip-bsps.jpg
    title:         "OSI ~ TCP/IP ~ Protokolle (Beispiele)"
    params:
      origin:       de.wikipedia.org
      originlink:   https://de.wikipedia.org/wiki/Internetprotokollfamilie
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      #attrby:
      #attrlink:

               # wikimedia.org
               # ##################################################

  - src:           wikimedia.org/IP_stack_connections.jpg
    title:         IP stack connections
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:IP_stack_connections_(corrected).svg
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      attrby:       Colin M.L. Burnett
      attrlink:     https://en.wikipedia.org/wiki/User:Cburnett


  - src:           wikimedia.org/UDP_encapsulation.jpg
    title:         IP stack connections
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:UDP_encapsulation.svg
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      attrby:       Kizar
      attrlink:     https://commons.wikimedia.org/wiki/User:Kizar


  - src:           wikimedia.org/NetworkTopologies.png
    title:         Network Topologies
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:NetworkTopologies.png
      license:      Public domain
      licenseAbrv:  public domain
      licenselink:  https://commons.wikimedia.org/wiki/Category:Public_domain
      attrby:       Foobaz
      attrlink:     https://commons.wikimedia.org/wiki/User:Foobaz

  - src:           wikimedia.org/NetworkTopology-Bus.png
    title:         Network Topology Bus
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:BusNetwork.svg
      license:      Public domain
      licenseAbrv:  public domain
      licenselink:  https://commons.wikimedia.org/wiki/Category:Public_domain
      attrby:       Foobaz
      attrlink:     https://commons.wikimedia.org/wiki/User:Foobaz

  - src:           wikimedia.org/NetworkTopology-Mesh.png
    title:         Network Topology Mesh
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:NetworkTopology-Mesh.svg
      license:      Public domain
      licenseAbrv:  public domain
      licenselink:  https://commons.wikimedia.org/wiki/Category:Public_domain
      attrby:       Foobaz
      attrlink:     https://commons.wikimedia.org/wiki/User:Foobaz

  - src:           wikimedia.org/NetworkTopology-Ring.png
    title:         Network Topology Ring
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:NetworkTopology-Ring.png
      license:      Public domain
      licenseAbrv:  public domain
      licenselink:  https://commons.wikimedia.org/wiki/Category:Public_domain
      attrby:       Foobaz
      attrlink:     https://commons.wikimedia.org/wiki/User:Foobaz

  - src:           wikimedia.org/NetworkTopology-Star.png
    title:         Network Topology Star
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:StarNetwork.svg
      license:      Public domain
      licenseAbrv:  public domain
      licenselink:  https://commons.wikimedia.org/wiki/Category:Public_domain
      attrby:       Foobaz
      attrlink:     https://commons.wikimedia.org/wiki/User:Foobaz

  - src:           wikimedia.org/NetworkTopology-Hybrid-1.jpg
    title:         Network Topology Hyprid
    params:
      origin:       wikimedia.org
      originlink:   "https://de.wikipedia.org/wiki/Topologie_(Rechnernetz)#Hybride_Topologien"
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      attrby:       Deadlyhappen

  - src:           wikimedia.org/NetworkTopology-Hybrid-2.jpg
    title:         Network Topology Hyprid
    params:
      origin:       wikimedia.org
      originlink:   "https://de.wikipedia.org/wiki/Topologie_(Rechnernetz)#Hybride_Topologien"
      license:      Creative Commons Attribution-ShareAlike 3.0 Unported License
      licenseAbrv:  WP:CC BY-SA
      licenselink:  https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
      attrby:       Deadlyhappen

  - src:           wikimedia.org/NetworkTopology-Peer2Peer.jpg
    title:         A peer-to-peer based network
    params:
      origin:       wikimedia.org
      originlink:   https://en.wikibooks.org/wiki/Network_Plus_Certification/Media_and_Topologies/Logical_Topologies
      license:      Public domain
      licenseAbrv:  public domain
      licenselink:  https://commons.wikimedia.org/wiki/Category:Public_domain

  - src:           wikimedia.org/NetworkTopology-ClientServer.jpg
    title:         A server based network
    params:
      origin:       wikimedia.org
      originlink:   https://en.wikibooks.org/wiki/Network_Plus_Certification/Media_and_Topologies/Logical_Topologies
      license:      Public domain
      licenseAbrv:  public domain
      licenselink:  https://commons.wikimedia.org/wiki/Category:Public_domain


  - src:           wikimedia.org/NetworkTopology-VPN.jpg
    title:         Virtual Private Network overview
    params:
      origin:       wikimedia.org
      originlink:   https://commons.wikimedia.org/wiki/File:Virtual_Private_Network_overview.svg
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       Ludovic.ferre
      attrlink:     https://commons.wikimedia.org/wiki/User:Ludovic.ferre


               # it-gro (screenshots)
               # ##################################################

  - src:           it-gro/courses.edx.org/**/*
    title:         courses.edx.org
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "256x128"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           it-gro/webminal.org/*
    title:         webminal
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "256x128"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           it-gro/datacamp.com/**/*
    title:         datacamp.com
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "256x128"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch


  - src:           it-gro/network/filius/homework/*.jpg
    title:         Filius Hausaufgabe
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           it-gro/network/filius/*.jpg
    title:         Filius Beispiele
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           it-gro/qr/*.jpg
    title:         rebrand.ly/cscfs19
    params:
      imgCmd:       "Fit"
      imgOpt:       "400x400"
      thumbCmd:     "Fit"
      thumbOpt:     "300x300"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       rebrandly.com
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           it-gro/mate/*.jpg
    title:         Mate Screenshot
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           it-gro/cloud/pizza-as-a-service*.jpg
    title:         Pizza as a Service
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Albert Barron
      originlink:   https://www.linkedin.com/pulse/20140730172610-9679881-pizza-as-a-service
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           it-gro/cloud/*.jpg
    title:         Cloud Computing
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       cloud.ods
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch


  - src:           it-gro/disk/*.jpg
    title:         Storage
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch


  - src:           it-gro/win10/*.jpg
    title:         Win10
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch


  - src:           it-gro/vagrant/*.jpg
    title:         vagrant
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot
      #originlink:
      license:      Creative Commons Attribution-ShareAlike 4.0 International
      licenseAbrv:  CC BY-SA 4.0
      licenselink:  https://creativecommons.org/licenses/by-sa/4.0/
      attrby:       it-gro
      attrlink:     https://it-grossniklaus.ch

  - src:           ey.com/*.jpg
    title:         EY
    params:
      imgCmd:       "Fit"
      imgOpt:       "1280x1024"
      thumbCmd:     "Fit"
      thumbOpt:     "1024x768"
      caption:      "%%F%%n%%O %%l%%n%%A"
      captionShort: "%%O%%n%%A"
      origin:       Screenshot, Pressemitteilung
      originlink:   https://www.ey.com/de/de/newsroom/news-releases/ey-20190705-boersenranking-us-amerikanische-digitalkonzerne-eilen-davon-deutsche-unternehmen-verlieren-an-bedeutung
      license:      (C) 2010 EYGM Limited 
      licenseAbrv:  (C) 2010 EYGM Limited 
      licenselink:  https://www.ey.com/de/de/newsroom/news-releases/ey-20190705-boersenranking-us-amerikanische-digitalkonzerne-eilen-davon-deutsche-unternehmen-verlieren-an-bedeutung
      attrby:       Ernst & Young GmbH
      attrlink:     https://www.ey.com/de/de/newsroom/news-releases/ey-20190705-boersenranking-us-amerikanische-digitalkonzerne-eilen-davon-deutsche-unternehmen-verlieren-an-bedeutung

  - src:            "slides/**"
    params:

---
