+++
title   = "SW01"
summary = "Semesterwoche 01"

css   = '''
.slide-background-content {
  // background-blend-mode: lighten;
  // background-color: rgba(255, 255, 255, 0.6)
  // background-blend-mode: darken;
  // background-color: rgba(0, 0, 20, 0.2);
  background-blend-mode: lighten;
  background-color: rgba(230, 230, 255, 0.6)
}
'''

[slides]
transition   = "fade"
theme        = "white"
chroma_style = "tango"
center       = false

# parallaxBackgroundImage       = "paper-1990111_640.jpg x2500"
parallaxBackgroundImage       = "grey-2715737_640.jpg"
parallaxBackgroundHorizontal  = 200
parallaxBackgroundVertical    = 10

+++

# Semesterwoche 1
## Kick-Off
[it-gro](https://it-grossniklaus.ch)

---

### Bruno Grossniklaus (1)

* Institut für Informatik und angewandte Mathematik (IAM) Universität Bern
  * lic. phil. nat. (M.Sc.)
  * Informatik
  * Mathematik
  * Betriebswirtschaft

---

### Bruno Grossniklaus (2)

* Freischaffender Informatiker und Dozent
* Für HSLU W.MS:
  * CSC
  * BDL1
  * BDL2
* Slogan (seit 1991):

{{% slides/fragment class="fade-in-then-semi-out" %}} *Daten leben länger als Apps*  {{% /slides/fragment %}}

---

### W.MSCIDS_BDL03.*.H1901

* 2 Session (Durchführungen)
  * SW01 .. SW 07 => W.MSCIDS_BDL03.1.H1901
  * SW08 .. SW 14 => W.MSCIDS_BDL03.2.H1901

{{% slides/fragment class="fade-in-then-semi-out" %}}
Die Sessions werden mit dem gleichen Inhalt durchgeführt
{{% /slides/fragment %}}


---

##  Agenda (1)

| SW#   | Topics                                      | Homework       |
|-------|---------------------------------------------|----------------|
| 01,08 | Introduction, Python SQLite3, CRUD          | ds-ub18-gui    |
| 02,09 | NoSQL, UML-Relations (1:1 1:n n:m), MariaDB |                |
| 03,10 | MongoDB: Introduction                       | DataCamp 1,2   |

---

##  Agenda (2)

| SW#   | Topics                                      | Homework       |
|-------|---------------------------------------------|----------------|
| 04,11 |                                             |                |
| 05,12 | MongoDB: GUIs, Cloud, PyMongo               | Cloud, PyMongo |
| 06,13 | MongoDB: Tour                               | MongoDB        |
| 07,14 | MongoDB: Finalisation                       | DataCamp 3,4   |

---

## Unterlagen

* ILIAS und Webseite (Link im ILIAS)

{{< slides/image "ilias-01.jpg"    "x300" >}}
{{< slides/image "website-01.jpg"  "x300" >}}


---

## Virtuelles System

* via Vagrant und VirtualBox erstellt

{{< slides/youtube id="mWPk0il3Z2c" >}}


---

## NoSQL
### Bedeutung nimmt schnell zu

<canvas class="stretch" data-chart="line">
             ,  Jan,   Feb,   Mrz,   Apr,   Mai,   Jun,   Jul,   Aug,   Sep,   Okt,  Nov,  Dez
2017         ,    0,     0,     0,     0,     0,     0,     0,     0,     0,    34, 31.1,  30.5
2018         , 30.0,  32.7,  46.2,  45.9,  47.3,  59.5,  62.0,  73.7,  85.2,  84.5, 85.7, 93.2
2019         , 92.6, 109.9, 154.8, 149.3, 148.0, 184.7, 166.4, 158.2, 160.4

<!--
{
 "data" : {
    "datasets" : [  { "backgroundColor": "#F00" }
                  , { "backgroundColor": "#0F0" }
                  , { "backgroundColor": "#00F" }
                 ]
  },
  "options": {
     "title": {
       "text"      : "NASDAQ: MDB (MongoDB, Inc.) High",
       "display"   : true,
       "fontSize"  : 20,
       "fontStyle" : "bold",
       "fontFamily": "Arial",
       "fontColor" : "black"
     },
     "legend": {
       "labels": {
       "display"   : true,
       "fontSize"  : 12,
       "fontStyle" : "italic",
       "fontFamily": "Arial",
       "fontColor" : "black"
       }
     }, 
     "scales": {
       "yAxes": [
         { 
           "ticks": {
             "beginAtZero": true,
             "fontColor": "black"
           }, 
           "scaleLabel": { 
             "display": true,
             "labelString": "high ($)",
             "fontSize"  : 12,
             "fontStyle" : "italic",
             "fontFamily": "Arial",
             "fontColor": "black"
           } 

         }
       ],
       "xAxes": [
         {
           "ticks": {
             "fontColor": "black"
           },
           "scaleLabel": { 
             "display": true,
             "labelString": "Monat",
             "fontSize"  : 12,
             "fontStyle" : "italic",
             "fontFamily": "Arial",
             "fontColor": "black"
           } 
           
         }
       ]
     } 
  }
}

-->
</canvas>

---

## Viel Erfolg

# :+1:
