+++
title   = "Showcase 2"
summary = "reveal.js with hugo shortchuts"

[slides]
#transition   = "none"
#transition   = "fade"
#transition   = "slide"
transition   = "convex"
#transition   = "concave"
#transition   = "zoom"

theme        = "white"
chroma_style = "tango"

# Choose a theme from https://github.com/hakimel/reveal.js#theming
# beige.css
# black.css
# blood.css
# league.css
# moon.css
# night.css
# serif.css
# simple.css
# sky.css
# solarized.css
# white.css

# autumn.css
# borland.css
# bw.css
# colorful.css
# default.css
# emacs.css
# friendly.css
# fruity.css
# manni.css
# monokai.css
# murphy.css
# native.css
# pastie.css
# perldoc.css
# rrt.css
# tango.css
# trac.css
# vim.css
# vs.css
+++


{{< slides/slide
  background-image="img/paper-1990111_640.jpg"
>}}

# reveal.js 
## meets Hugo
### shortchuts

[it-gro](https://it-grossniklaus.ch)

---

##  slides/image
{{< slides/image "paper-1990111_640.jpg" >}}

---

##  slides/image

{{< slides/image "keyboard/100-macOS-kbd-osd-1.jpg" >}}
{{< slides/image "keyboard/100-macOS-kbd-osd-1.jpg" "100x"    >}}
{{< slides/image "keyboard/100-macOS-kbd-osd-1.jpg" "200x300" >}}
{{< slides/image "keyboard/100-macOS-kbd-osd-1.jpg" "200x200" "Fit"  >}}
{{< slides/image "keyboard/100-macOS-kbd-osd-1.jpg" "200x100" "Fill" >}}

---


## Transition
### FrontMatter

    transition   = 
    
none, fade, slide, convex, concave, zoom

\

* [transitions](https://revealjs.com/?transition=fade#/transitions)
* [reveal.js#configuration](https://github.com/hakimel/reveal.js#configuration)


---

{{< slides/slide
    transition="zoom-in slide-out"
>}}

## Transition

    {{</* slides/slide transition="zoom-in slide-out" */>}}
    
* *-in
* *-out

\

* [reveal.js#configuration](https://github.com/hakimel/reveal.js#configuration)

--- 

## slides/youtube

    {{</*  slides/youtube id="rhlERPQG6sQ" */>}}

{{<  slides/youtube id="rhlERPQG6sQ" >}}


--- 

## slides/youtube

```
{{</* slides/youtube 
    id="rhlERPQG6sQ" 
    width="900" 
    height="500" 
    autoplay=1 
*/>}}
```

---

{{< slides/slide
    background="#00FF00" 
    background-transition="zoom"
>}}


## slides/slide

```
{{</* slides/slide
    background-color="#00FF00" 
    background-transition="zoom"
*/>}}
```

---

{{< slides/slide
    background-video="https://s3.amazonaws.com/static.slid.es/site/homepage/v1/homepage-video-editor.mp4,https://s3.amazonaws.com/static.slid.es/site/homepage/v1/homepage-video-editor.webm"
>}}

---

## slides/fragment

* [reveal.js#fragments](https://github.com/hakimel/reveal.js#fragments)

* Lorem ipsum

{{% slides/fragment %}} default {{% /slides/fragment %}}

---

## slides/fragment

* Lorem ipsum

{{% slides/fragment class="highlight-current-blue"  %}} highlight-current-blue{{% /slides/fragment %}}
{{% slides/fragment class="highlight-red         "  %}} highlight-red         {{% /slides/fragment %}}
{{% slides/fragment class="highlight-green       "  %}} highlight-green       {{% /slides/fragment %}}
{{% slides/fragment class="highlight-blue        "  %}} highlight-blue        {{% /slides/fragment %}}

---

## slides/fragment

* Lorem ipsum

{{% slides/fragment class="fade-up               "  %}} fade-up               {{% /slides/fragment %}}
{{% slides/fragment class="fade-out              "  %}} fade-out              {{% /slides/fragment %}}
{{% slides/fragment class="fade-in-then-out      "  %}} fade-in-then-out      {{% /slides/fragment %}}
{{% slides/fragment class="fade-in-then-semi-out "  %}} fade-in-then-semi-out {{% /slides/fragment %}}


---

## slides/fragment

{{% slides/fragment class="fade-in-then-semi-out" %}} {{< slides/image "keyboard/100-macOS-kbd-osd-1.jpg" "200x" >}} {{% /slides/fragment %}}
{{% slides/fragment class="fade-in-then-semi-out" %}} {{< slides/image "keyboard/100-macOS-kbd-osd-1.jpg" "200x" >}} {{% /slides/fragment %}}

---

## slides/speaker_note

```markdown
{{%/* slides/speaker_note */%}}
- Only the speaker can read these notes
- Press `S` key to view
{{%/* /slides/speaker_note */%}}
```

Press the `S` key to view the speaker notes!

{{< slides/speaker_note >}}
- Only the speaker can read these notes
- Press `S` key to view
{{< /slides/speaker_note >}}


