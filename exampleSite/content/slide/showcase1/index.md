+++
title   = "Showcase 1"
summary = "reveal.js with hugo markdown"

[slides]
#transition   = "none"
#transition   = "fade"
#transition   = "slide"
transition   = "convex"
#transition   = "concave"
#transition   = "zoom"

theme        = "white"
chroma_style = "tango"

# Choose a theme from https://github.com/hakimel/reveal.js#theming
# beige.css
# black.css
# blood.css
# league.css
# moon.css
# night.css
# serif.css
# simple.css
# sky.css
# solarized.css
# white.css

# autumn.css
# borland.css
# bw.css
# colorful.css
# default.css
# emacs.css
# friendly.css
# fruity.css
# manni.css
# monokai.css
# murphy.css
# native.css
# pastie.css
# perldoc.css
# rrt.css
# tango.css
# trac.css
# vim.css
# vs.css
+++


{{< slides/slide
  background-image="img/paper-1990111_640.jpg"
>}}

# reveal.js 
## meets Hugo
### Markdown

[it-gro](https://it-grossniklaus.ch)

---

##  Table

| Tool     | url                                                               |
|----------|-------------------------------------------------------------------|
| revealjs | https://revealjs.com                                              |
|          | https://github.com/hakimel/reveal.js                              |
|          | [transitions](https://revealjs.com/?transition=fade#/transitions) |
|          | [themes](https://revealjs.com/?transition=zoom#/themes)           |
| hugo     | https://gohugo.io                                                 |

---

##  Text

This __is__ **bold**

This _is_ _italic_

This ___is___  ***both***

* [#bold](https://www.markdownguide.org/basic-syntax/#bold)
* [#italic](https://www.markdownguide.org/basic-syntax/#italic)
* [#bold-and-italic](https://www.markdownguide.org/basic-syntax/#bold-and-italic)

---

##  Flow

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua.

At vero eos et accusam et justo duo dolores et ea rebum. Stet clita
kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
amet.

---

##  Flow

Lorem ipsum dolor sit amet, consetetur       
sadipscing elitr, sed diam nonumy eirmod     
tempor invidunt ut labore et dolore		     
magna aliquyam erat, sed diam			     
voluptua. 

* [#paragraphs-1](https://www.markdownguide.org/basic-syntax/#paragraphs-1)

---

## Blockquotes

> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
> nonumy eirmod tempor invidunt ut labore et dolore

* [#blockquotes-1](https://www.markdownguide.org/basic-syntax/#blockquotes-1)

---


##  Bullets
* bullet 1
* bullet 2
  * bullet 2a
  * bullet 2b
* bullet 3

\

* [#unordered-lists](https://www.markdownguide.org/basic-syntax/#unordered-lists)

---

##  Numbered Lists
1. list 1
1. list 2
   1. list 2.1
   1. list 2.2
1. list 3

\

* [#ordered-lists](https://www.markdownguide.org/basic-syntax/#ordered-lists)

---

##  Code

Check variable `foo` 

    a=b
	a++

```bash
#!/usr/bin/env bash
foo="bar"
```

```python
porridge = "blueberry"
if porridge == "blueberry":
    print("Eating...")
```

* [#code-blocks-1](https://www.markdownguide.org/basic-syntax/#code-blocks-1)
* [#code](https://www.markdownguide.org/basic-syntax/#code)

---

## Images
![paper](img/paper-1990111_640.jpg)

* [#images-1](https://www.markdownguide.org/basic-syntax/#images-1)
