+++
title   = "Showcase 3"
summary = "reveal.js with hugo externals"

css   = '''
.slide-background-content {
  // background-blend-mode: lighten;
  // background-color: rgba(255, 255, 255, 0.6)
  background-blend-mode: darken;
  background-color: rgba(0, 8, 0, 0.9)
}
'''

[slides]
transition   = "convex"
#theme        = "white"
theme        = "black"
chroma_style = "tango"

#width   	 = "100%"
#height		 = "100%"
#margin		 = 0
#minScale	 = 1
#maxScale	 = 1

#parallaxBackgroundImage      = "https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg"
#parallaxBackgroundImage       = "red-2925963.jpg x900 Resize"
#parallaxBackgroundImage       = "blur-3280300.jpg x900"
parallaxBackgroundImage       = "seamless-2033661_640.jpg"
parallaxBackgroundHorizontal  = 200
parallaxBackgroundVertical    = 10


# autoSlide    = 3000
# loop         = true
# shuffle      = true
# slideNumber  = true
+++


# reveal.js 
## meets Hugo
### Externals

[it-gro](https://it-grossniklaus.ch)


---

# Charts


<canvas data-chart="line">
<!-- 
{
 "data": {
  "labels": ["January"," February"," March"," April"," May"," June"," July"],
  "datasets": [
   {
    "data":[65,59,80,81,56,55,40],
    "label":"My first dataset","backgroundColor":"rgba(20,220,220,.8)"
   },
   {
    "data":[28,48,40,19,86,27,90],
    "label":"My second dataset","backgroundColor":"rgba(220,120,120,.8)"
   }
  ]
 }, 
 "options": { "responsive": "true" }
}
-->
</canvas>

[chartjs.org](https://www.chartjs.org), [reveal.js-plugins](https://github.com/rajgoel/reveal.js-plugins)

---

# Charts

<canvas class="stretch" data-chart="line">
My first dataset, 65, 59, 80, 81, 56, 55, 40
<!-- This is a comment that will be ignored -->
My second dataset, 28, 48, 40, 19, 86, 27, 90
<!-- 
{ 
 "data" : {
  "labels" : ["Enero", "Febrero", "Marzo", "Avril", "Mayo", "Junio", "Julio"],
  "datasets" : [{ "borderColor": "#0f0", "borderDash": ["5","10"] }, { "borderColor": "#0ff" } ]
 }
}
-->
</canvas>


---

# Charts

<canvas class="stretch" data-chart="bar">
,January, February, March, April, May, June, July
My first dataset, 65, 59, 80, 81, 56, 55, 40
My second dataset, 28, 48, 40, 19, 86, 27, 90
</canvas>
				

---

# Charts

<canvas class="stretch" data-chart="pie">
,Black, Red, Green, Yellow
My first dataset, 40, 40, 20, 6
My second dataset, 45, 40, 25, 4
</canvas>
				
---

# Charts

<canvas class="stretch" data-chart="radar">
,Black, Red, Green, Yellow
My first dataset, 40, 40, 20, 6
My second dataset, 45, 40, 25, 4
</canvas>
				

---

# Charts
<canvas data-chart="line">
Month, January, February, March, April, May, June, July
My first dataset, 65, 59, 80, 81, 56, 55, 40
My second dataset, 28, 48, 40, 19, 86, 27, 90
</canvas>

---

# Charts
<canvas data-chart="doughnut">
Month, January, February, March, April, May, June, July
My first dataset, 65, 59, 80, 81, 56, 55, 40
My second dataset, 28, 48, 40, 19, 86, 27, 90
</canvas>


---

---

## Theorie vs Labor
<canvas class="stretch" data-chart="radar">
                , SW01, SW02, SW03, SW04, SW05, SW06, SW07, SW08, SW09, SW10, SW11, SW12, SW13, SW14
Theorie         ,    3,    1,    1,    3, 1.5,     1,    1,    1,    1,    1,    1,    1,    1,    1
Labor/Hands-On  ,    0,    2,    2,    0, 1.5,     2,    2,    2,    2,    2,    2,    2,    2,    2
<!--
{
 "data" : {
   "datasets" : [
      {
        "backgroundColor": [
                               "hsla( 140,  90%,  80%, 0.3)"
                           ]
      },
      {
        "backgroundColor": [
                               "hsla( 200,  90%,  80%, 0.6)"
                           ]
      }
    ]
  },
  "options": {
     "title": {
       "text"      : "3 ECTS => 90 Std",
       "display"   : true,
       "fontSize"  : 18,
       "fontStyle" : "bold",
       "fontFamily": "Arial",
       "fontColor" : "black"
     },
     "legend": {
       "labels": {
       "display"   : true,
       "fontSize"  : 18,
       "fontStyle" : "bold",
       "fontFamily": "Arial",
       "fontColor" : "black"
       }
     },
     "scale": {
       "xAxes": [
         { 
           "ticks": {
             "beginAtZero": true,
             "fontColor": "black"
           }, 
           "scaleLabel": { 
             "display": true,
             "labelString": "high ($)",
             "fontSize"  : 20,
             "fontStyle" : "italic",
             "fontFamily": "Arial",
             "fontColor": "black"
           } 

         }
       ]
     }
  }
}
-->
</canvas>

---


## Math

* https://www.mathjax.org

In-line math: $x + y = z$

Block math:

$$
f\left( x \right) = \;\frac{{2\left( {x + 4} \right)\left( {x - 4} \right)}}{{\left( {x + 4} \right)\left( {x + 1} \right)}}
$$

---

# Gravizo

* http://gravizo.com

\

* DOT (graph description language)
* Sequence Diagrams (PlantUML)
* Use Case Diagrams (PlantUML)
* Activity Diagrams (PlantUML)
* Component Diagrams (PlantUML)
* Class Diagrams (UMLGraph)


---

### Gravizo digraph

{{< slides/gravizo "DOT Language (GraphViz) Example" >}}
  digraph G {
    aize ="4,4";
    main [shape=box];
    main -> parse [weight=8];
    parse -> execute;
    main -> init [style=dotted];
    main -> cleanup;
    execute -> { make_string; printf}
    init -> make_string;
    edge [color=red];
    main -> printf [style=bold,label="100 times"];
    make_string [label="make a string"];
    node [shape=box,style=filled,color=".7 .3 1.0"];
    execute -> compare;
  }
{{< /slides/gravizo >}}


---

### Gravizo startuml

{{< slides/gravizo "FooBar" >}}
@startuml;


'skinparam monochrome true
'skinparam monochrome reverse
'skinparam handwritten true

skinparam   BackgroundColor #333333
'skinparam   FontName        Arial
'skinparam   FontSize        24
'skinparam   FontStyle       normal
'skinparam   FontColor       #FF0000

title _

skinparam title {
  FontSize           26
  FontColor          #333333
}

skinparam default {
  FontName        Arial
  FontSize        16
  FontColor       #FFFFFF
}

skinparam usecase {
  FontName        Arial
  FontSize        24
  FontStyle       normal
  FontColor       #FFFFFF
  BackgroundColor #000000
  BorderColor     #FFFFFF
  ArrowColor      #FFFFFF
  'ArrowThickness  2
}


skinparam actor {
  FontName        Arial
  FontSize        20
  FontStyle       normal
  FontColor       #FFFFFF
  BorderColor     #FFFFFF
  BackgroundColor brown
}


User -> (Start)
User --> (Use the application) : A small label

:Main Admin: ---> (Use the application) : This is\nyet another\nlabel

@enduml
{{< /slides/gravizo >}}

---

### Gravizo startuml

{{<  slides/gravizo "NETT" >}}
@startuml;

note top of vagvbx
Projekt aus github
end note

package "vag-vbx-nett" as vagvbx {

  ' -------------------------------------------------- 
  note top of win2019
  Windows Server 2019
  end note
   
  ' MediumBlue
   
  folder win2019 #AliceBlue95   {
    component win2019server #AliceBlue85  [
                        nett-win2019-server 
                        172.24.40.20/22
		                    DNS-Server
		                    Webserver (IIS)
		                    Fileserver
		]

    component win2019client #AliceBlue90  [
                        nett-win2019-client
                        172.24.41.5/22
		]

	
	note bottom of win2019client
	Browser (Chrome)
  end note

  }

  ' -------------------------------------------------- 
  note top of ub18
  Ubuntu 18.04
  end note
   
  ' Mangenta
  folder ub18 #BlueViolet95 {
   	component ub18cli #BlueViolet90 [
                        nett-ub18-cli
                        172.24.41.3/22
                        CLI Tools
                      ]

   	component ub18www #BlueViolet85 [
                        nett-ub18-www
                        172.24.40.10/22
                        Webserver (Apache)
                      ]

   	component ub18proxy #BlueViolet85 [
                        nett-ub18-proxy
                        172.24.40.11/22
                        Proxy (HAProxy)
                      ]

   	component ub18db #BlueViolet85 [
                        nett-ub18-db
                        172.24.40.12/22
                        DB-Server (MariaDB)
                        phpmyadmin
                      ]

   	component ub18gui #BlueViolet90 [
                        nett-ub18-gui
                        172.24.41.4/22
                        GUI
                      ]

	note bottom of ub18gui
	Browser (Chrome)
  Wireshark
	Filius
  end note


  }


}
@enduml

{{< /slides/gravizo >}}

